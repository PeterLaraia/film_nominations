tt2562232;Birdman: Or (The Unexpected Virtue of Ignorance);2014;Comedy;Alejandro;Iñárritu;English;USA;1959-2-11
tt1065073;Boyhood;2014;Drama;Richard;Linklater;English;USA;1969-5-19
tt2278388;The Grand Budapest Hotel;2014;Adventure;Wes;Anderson;English;USA;1956-5-30
tt2084970;The Imitation Game;2014;Biography;Morten;Tyldum;English;UK;1954-7-14
tt2179136;American Sniper;2014;Action;Clint;Eastwood;English;USA;1969-3-21
tt2980516;The Theory of Everything;2014;Biography;James;Marsh;English;UK;1965-4-17
tt2582802;Whiplash;2014;Drama;Damien;Chazelle;English;USA;1964-10-12
tt1020072;Selma;2014;Biography;Ava;DuVernay;English;UK;1960-3-2
tt1100089;Foxcatcher;2014;Biography;Bennett;Miller;English;USA;1972-2-6
tt3316960;Still Alice;2014;Drama;Richard;Glatzer;English;USA;1950-12-14
tt2737050;Two Days, One Night;2014;Drama;Jean-Pierre;Dardenne;French;Belgium;1955-9-29
tt2267998;Gone Girl;2014;Drama;David;Fincher;English;USA;1954-3-16
tt0758758;Into the Wild;2007;Adventure;Sean;Penn;English;USA;1950-12-22
tt1872194;The Judge;2014;Drama;David;Dobkin;English;USA;1970-5-19
tt2180411;Into the Woods;2014;Adventure;Rob;Marshall;English;USA;1952-5-3
tt2718492;Ida;2013;Drama;Pawel;Pawlikowski;Polish;Poland;1970-3-23
tt3011894;Wild Tales;2014;Comedy;Damián;Szifrón;Spanish;Argentina;1941-4-26
tt2802154;Leviathan;2014;Drama;Andrey;Zvyagintsev;Russian;Russia;1978-7-5
tt3409392;Timbuktu;2014;Drama;Abderrahmane;Sissako;French;France;1977-5-29
tt2991224;Tangerines;2013;Drama;Zaza;Urushadze;Estonian;Estonia;1972-5-14
tt2872718;Nightcrawler;2014;Crime;Dan;Gilroy;English;USA;1948-3-30
tt1791528;Inherent Vice;2014;Crime;Paul;Anderson;English;USA;1940-5-19
tt2473794;Mr. Turner;2014;Biography;Mike;Leigh;English;UK;1975-6-27
tt1809398;Unbroken;2014;Biography;Angelina;Jolie;English;USA;1947-10-24
tt0816692;Interstellar;2014;Adventure;Christopher;Nolan;English;USA;1949-4-4
tt1490017;The Lego Movie;2014;Animation;Phil;Lord;English;Australia;1947-7-12
tt1980929;Begin Again;2013;Drama;John;Carney;English;USA;1952-7-23
tt3125324;Beyond the Lights;2014;Drama;Gina;Prince-Bythewood;English;USA;1965-2-21
tt2049586;Glen Campbell: I'll Be Me;2014;Documentary;James;Keach;English;USA;1954-10-22
tt1587310;Maleficent;2014;Action;Robert;Stromberg;English;USA;1978-12-3
tt2015381;Guardians of the Galaxy;2014;Action;James;Gunn;English;USA;1971-4-1
tt2310332;The Hobbit: The Battle of the Five Armies;2014;Adventure;Peter;Jackson;English;New Zealand;1962-12-14
tt1843866;Captain America: The Winter Soldier;2014;Action;Anthony;Russo;English;USA;1941-2-1
tt2103281;Dawn of the Planet of the Apes;2014;Action;Matt;Reeves;English;USA;1979-8-17
tt1877832;X-Men: Days of Future Past;2014;Action;Bryan;Singer;English;USA;1964-2-8
tt2245084;Big Hero 6;2014;Animation;Don;Hall;English;USA;1980-4-27
tt0787474;The Boxtrolls;2014;Animation;Graham;Annable;English;USA;1944-5-11
tt1646971;How to Train Your Dragon 2;2014;Animation;Dean;DeBlois;English;USA;1945-12-23
tt1865505;Song of the Sea;2014;Animation;Tomm;Moore;English;Ireland;1974-7-4
tt2576852;The Tale of The Princess Kaguya;2013;Animation;Isao;Takahata;Japanese;Japan;1959-4-2
tt0426459;Feast;2005;Action;John;Gulager;English;USA;1980-5-12
tt3610188;The Bigger Picture;2014;Animation;Daisy;Jacobs;English;UK;1956-5-15
tt3326262;The Dam Keeper;2014;Animation;Robert;Kondo;English;USA;1967-10-25
tt3838700;A Single Life;2014;Animation;Marieke;Blaauw;N/A;Netherlands;1967-10-24
tt3946086;Me and My Moulton;2014;Animation;Torill;Kove;English;Norway;1959-6-3
tt4044364;Citizenfour;2014;Documentary;Laura;Poitras;English;Germany;1943-7-22
tt2714900;Finding Vivian Maier;2013;Documentary;John;Maloof;English;USA;1960-1-20
tt3279124;Last Days in Vietnam;2014;Documentary;Rory;Kennedy;English;USA;1947-11-23
tt3674140;The Salt of the Earth;2014;Documentary;Juliano;Salgado;French;France;1952-4-15
tt3455224;Virunga;2014;Documentary;Orlando;Einsiedel;English;UK;1976-9-6
tt3449252;Crisis Hotline: Veterans Press 1;2013;Documentary;Ellen;Kent;English;USA;1973-4-7
tt1799110;Joanna;2010;Drama;Feliks;Falk;Polish;Poland;1958-4-3
tt3069992;Our Curse;2013;Documentary;Tomasz;Sliwinski;Polish;Poland;1973-4-26
tt1523317;The Lady and the Reaper (La dama y la muerte);2009;Animation;Javier;Gracia;English;Spain;1943-3-25
tt3044564;White Earth;2014;Documentary;Christian;Jensen;English;USA;1968-5-6
tt3071532;The Phone Call;2013;Short;Mat;Kirkby;English;UK;1955-10-1
tt2326094;Aya;2012;Short;Oded;Binnun;English;France;1952-5-25
tt3612232;Boogaloo and Graham;2014;Short;Michael;Lennox;English;UK;1951-8-18
tt2922078;Butter Lamp;2013;Short;Wei;Hu;Tibetan;France;1965-3-18
tt3008010;Parvaneh;2012;Short;Talkhon;Hamzavi;German;Switzerland;1944-8-1
tt2024544;12 Years a Slave;2013;Biography;Steve;McQueen;English;USA;1955-10-23
tt1535109;Captain Phillips;2013;Biography;Paul;Greengrass;English;USA;1969-5-10
tt0790636;Dallas Buyers Club;2013;Biography;Jean-Marc;Vallée;English;USA;1950-1-22
tt1454468;Gravity;2013;Sci-Fi;Alfonso;Cuarón;English;USA;1943-10-11
tt1798709;Her;2013;Drama;Spike;Jonze;English;USA;1978-11-21
tt1821549;Nebraska;2013;Comedy;Alexander;Payne;English;USA;1953-6-16
tt2431286;Philomena;2013;Biography;Stephen;Frears;English;UK;1958-9-17
tt1800241;American Hustle;2013;Crime;David;Russell;English;USA;1971-8-10
tt0993846;The Wolf of Wall Street;2013;Biography;Martin;Scorsese;English;USA;1964-6-15
tt2334873;Blue Jasmine;2013;Drama;Woody;Allen;English;USA;1946-9-23
tt1322269;August: Osage County;2013;Drama;John;Wells;English;USA;1966-6-12
tt2358891;The Great Beauty;2013;Drama;Paolo;Sorrentino;Italian;Italy;1968-7-13
tt2024519;The Broken Circle Breakdown;2012;Drama;Felix;Groeningen;Flemish;Belgium;1947-6-19
tt2852470;The Missing Picture;2013;Documentary;Rithy;Panh;French;Cambodia;1944-2-19
tt0099810;The Hunt for Red October;1990;Action;John;McTiernan;English;USA;1940-10-19
tt2852406;Omar;2013;Drama;Hany;Abu-Assad;Arabic;Palestine;1946-4-2
tt2209418;Before Midnight;2013;Drama;Richard;Linklater;English;USA;1961-6-25
tt2042568;Inside Llewyn Davis;2013;Drama;Ethan;Coen;English;USA;1959-6-8
tt1392214;Prisoners;2013;Crime;Denis;Villeneuve;English;USA;1961-4-8
tt1462900;The Grandmaster;2013;Action;Kar;Wong;Mandarin;Hong Kong;1955-11-17
tt0816442;The Book Thief;2013;Drama;Brian;Percival;English;USA;1947-10-7
tt2140373;Saving Mr. Banks;2013;Biography;John;Hancock;English;USA;1977-5-24
tt2294629;Frozen;2013;Animation;Chris;Buck;English;USA;1962-12-1
tt1690953;Despicable Me 2;2013;Animation;Pierre;Coffin;English;USA;1967-10-15
tt2304771;Mandela: Long Walk to Freedom;2013;Biography;Justin;Chadwick;English;UK;1971-9-18
tt1343092;The Great Gatsby;2013;Drama;Baz;Luhrmann;English;Australia;1977-3-7
tt1700845;The Invisible Woman;2013;Biography;Ralph;Fiennes;English;UK;1943-11-24
tt3063516;Jackass Presents: Bad Grandpa;2013;Comedy;Jeff;Tremaine;English;USA;1978-1-2
tt1210819;The Lone Ranger;2013;Action;Gore;Verbinski;English;USA;1960-10-29
tt1170358;The Hobbit: The Desolation of Smaug;2013;Adventure;Peter;Jackson;English;USA;1954-10-5
tt1091191;Lone Survivor;2013;Action;Peter;Berg;English;USA;1966-2-13
tt2017038;All Is Lost;2013;Action;J.C.;Chandor;English;USA;1967-9-15
tt1300854;Iron Man 3;2013;Action;Shane;Black;English;USA;1976-11-30
tt1408101;Star Trek Into Darkness;2013;Action;J.J.;Abrams;English;USA;1947-7-11
tt0481499;The Croods;2013;Animation;Kirk;Micco;English;USA;1976-3-15
tt1816518;Ernest & Celestine;2012;Animation;Stéphane;Aubier;French;France;1959-8-5
tt2013293;The Wind Rises;2013;Animation;Hayao;Miyazaki;Japanese;Japan;1946-6-12
tt3043542;Mr Hublot;2013;Animation;Alexandre;Espigares;English;Luxembourg;1974-8-1
tt2484120;Room on the Broom;2012;Animation;Jan;Lachauer;English;UK;1974-12-21
tt2499022;Feral;2012;Animation;Daniel;Sousa;English;USA;1965-8-5
tt0171259;Earthly Possessions;1999;Crime;James;Lapine;English;USA;1957-9-11
tt2396566;Twenty Feet from Stardom;2013;Documentary;Morgan;Neville;English;USA;1968-2-26
tt2375605;The Act of Killing;2012;Documentary;Joshua;Oppenheimer;Indonesian;Denmark;1947-7-27
tt2355540;Cutie and the Boxer;2013;Documentary;Zachary;Heinzerling;English;USA;1956-2-20
tt2532528;Dirty Wars;2013;Documentary;Rick;Rowley;English;USA;1954-12-21
tt2486682;The Square;2013;Documentary;Jehane;Noujaim;Arabic;Egypt;1941-5-28
tt2924484;The Lady in Number 6: Music Saved My Life;2013;Documentary;Malcolm;Clarke;English;Canada;1954-12-6
tt2516860;Cavedigger;2013;Documentary;Jeffrey;Karoff;English;USA;1952-6-22
tt2776636;Facing Fear;2013;Documentary;Jason;Cohen;English;USA;1959-6-27
tt2551790;Karama Has No Walls;2012;Documentary;Sara;Ishaq;Arabic;United Arab Emirates;1969-3-17
tt3145026;Prison Terminal: The Last Days of Private Jack Hall;2013;Documentary;Edgar;Barens;English;USA;1942-8-15
tt3346410;Helium;2013;Short;Anders;Walter;Danish;Denmark;1953-5-19
tt2372213;That Wasn't Me;2012;Short;Esteban;Crespo;Spanish;Spain;1965-5-30
tt2689992;Just Before Losing Everything;2013;Short;Xavier;Legrand;French;France;1944-10-3
tt2256646;Do I Have to Take Care of Everything?;2012;Short;Selma;Vilhunen;Finnish;Finland;1943-6-7
tt1998355;The Voorman Problem;2011;Short;Mark;Gill;English;UK;1960-9-9
tt1024648;Argo;2012;Drama;Ben;Affleck;English;USA;1943-10-2
tt1570728;Crazy, Stupid, Love.;2011;Comedy;Glenn;Ficarra;English;USA;1955-10-30
tt2125435;Beasts of the Southern Wild;2012;Drama;Benh;Zeitlin;English;USA;1973-1-4
tt1853728;Django Unchained;2012;Western;Quentin;Tarantino;English;USA;1963-3-24
tt1707386;Les Misérables;2012;Drama;Tom;Hooper;English;USA;1961-4-3
tt0454876;Life of Pi;2012;Adventure;Ang;Lee;English;USA;1967-7-11
tt0443272;Lincoln;2012;Biography;Steven;Spielberg;English;USA;1966-8-14
tt1045658;Silver Linings Playbook;2012;Comedy;David;Russell;English;USA;1969-2-9
tt1790885;Zero Dark Thirty;2012;Drama;Kathryn;Bigelow;English;USA;1962-2-2
tt0311113;Master and Commander: The Far Side of the World;2003;Action;Peter;Weir;English;USA;1945-2-21
tt1907668;Flight;2012;Drama;Robert;Zemeckis;English;USA;1959-9-15
tt1649419;The Impossible;2012;Drama;J.A.;Bayona;English;Spain;1958-1-26
tt1866249;The Sessions;2012;Biography;Ben;Lewin;English;USA;1973-9-25
tt0477348;No Country for Old Men;2007;Crime;Ethan;Coen;English;USA;1965-3-18
tt1276419;A Royal Affair;2012;Drama;Nikolaj;Arcel;Danish;Denmark;1980-1-17
tt1820488;War Witch;2012;Drama;Kim;Nguyen;French;Canada;1962-3-8
tt1613750;Kon-Tiki;2012;Adventure;Joachim;Rønning;Norwegian;UK;1971-7-24
tt1748122;Moonrise Kingdom;2012;Adventure;Wes;Anderson;English;USA;1943-4-9
tt1781769;Anna Karenina;2012;Drama;Joe;Wright;English;UK;1958-9-28
tt1074638;Skyfall;2012;Action;Sam;Mendes;English;UK;1960-9-16
tt1579361;Chasing Ice;2012;Documentary;Jeff;Orlowski;English;USA;1956-4-28
tt1637725;Ted;2012;Comedy;Seth;MacFarlane;English;USA;1968-10-31
tt0903624;The Hobbit: An Unexpected Journey;2012;Adventure;Peter;Jackson;English;USA;1979-1-29
tt1667353;Mirror Mirror;2012;Adventure;Tarsem;Singh;English;USA;1941-10-17
tt1735898;Snow White and the Huntsman;2012;Action;Rupert;Sanders;English;USA;1966-8-3
tt0975645;Hitchcock;2012;Biography;Sacha;Gervasi;English;USA;1971-10-15
tt0848228;The Avengers;2012;Action;Joss;Whedon;English;USA;1973-4-16
tt1446714;Prometheus;2012;Adventure;Ridley;Scott;English;USA;1946-7-30
tt1217209;Brave;2012;Animation;Mark;Andrews;English;USA;1966-2-11
tt1144951;Frankenweenie;2012;Animation;Tim;Burton;English;USA;1976-8-3
tt1623288;ParaNorman;2012;Animation;Chris;Butler;English;USA;1956-6-21
tt1430626;The Pirates! Band of Misfits;2012;Animation;Peter;Lord;English;UK;1969-11-6
tt1772341;Wreck-It Ralph;2012;Animation;Rich;Moore;English;USA;1980-4-11
tt2388725;Paperman;2012;Animation;John;Kahrs;English;USA;1977-10-31
tt2162565;Adam and Dog;2011;Animation;Minkyu;Lee;N/A;USA;1966-12-16
tt2309977;Fresh Guacamole;2012;Animation;PES;@@@;English;USA;1977-6-13
tt0192111;Head Over Heels;2001;Comedy;Mark;Waters;English;USA;1951-7-12
tt2125608;Searching for Sugar Man;2012;Documentary;Malik;Bendjelloul;English;Sweden;1964-12-19
tt2309788;The Gatekeepers;2012;Documentary;Dror;Moreh;Hebrew;Israel;1968-6-4
tt2124803;How to Survive a Plague;2012;Documentary;David;France;English;USA;1956-12-2
tt2120152;The Invisible War;2012;Documentary;Kirby;Dick;English;USA;1980-5-14
tt2123210;Inocente;2012;Documentary;Sean;Fine;English;USA;1977-11-26
tt2109153;Kings Point;2012;Documentary;Sari;Gilman;English;USA;1973-12-20
tt2083264;Mondays at Racine;2012;Documentary;Cynthia;Wade;English;USA;1973-7-28
tt2088962;An Open Heart;2012;Drama;Marion;Laine;French;France;1979-3-11
tt0111161;The Shawshank Redemption;1994;Crime;Frank;Darabont;English;USA;1944-12-15
tt2088735;Curfew;2012;Short;Shawn;Christensen;English;USA;1979-11-11
tt2136747;Asad;2012;Short;Bryan;Buckley;Somali;South Africa;1976-3-1
tt2133304;Buzkashi Boys;2012;Short;Sam;French;Dari;Afghanistan;1960-2-28
tt2312702;Death of a Shadow;2012;Short;Tom;Avermaet;Flemish;Belgium;1946-1-23
tt0097499;Henry V;1989;Action;Kenneth;Branagh;English;UK;1956-12-1
tt1655442;The Artist;2011;Comedy;Michel;Hazanavicius;English;France;1971-11-19
tt0970179;Hugo;2011;Adventure;Martin;Scorsese;English;USA;1954-10-22
tt1210166;Moneyball;2011;Biography;Bennett;Miller;English;USA;1954-8-19
tt1033575;The Descendants;2011;Comedy;Alexander;Payne;English;USA;1966-1-10
tt0478304;The Tree of Life;2011;Drama;Terrence;Malick;English;USA;1965-3-12
tt1605783;Midnight in Paris;2011;Comedy;Woody;Allen;English;Spain;1954-12-30
tt1454029;The Help;2011;Drama;Tate;Taylor;English;USA;1972-6-14
tt1568911;War Horse;2011;Drama;Steven;Spielberg;English;USA;1970-7-24
tt1554091;A Better Life;2011;Drama;Chris;Weitz;English;USA;1958-2-4
tt1340800;Tinker Tailor Soldier Spy;2011;Drama;Tomas;Alfredson;English;France;1945-12-16
tt1007029;The Iron Lady;2011;Biography;Phyllida;Lloyd;English;UK;1978-1-14
tt1602098;Albert Nobbs;2011;Drama;Rodrigo;García;English;UK;1964-10-4
tt1568346;The Girl with the Dragon Tattoo;2011;Crime;David;Fincher;English;USA;1960-10-28
tt1655420;My Week with Marilyn;2011;Biography;Simon;Curtis;English;UK;1945-9-14
tt1532503;Beginners;2010;Comedy;Mike;Mills;English;USA;1957-7-20
tt1291584;Warrior;2011;Drama;Gavin;O'Connor;English;USA;1961-4-1
tt1478338;Bridesmaids;2011;Comedy;Paul;Feig;English;USA;1942-8-28
tt1821593;Bullhead;2011;Crime;Michaël;Roskam;Flemish;Belgium;1965-2-17
tt1445520;Footnote;2011;Comedy;Joseph;Cedar;Hebrew;Israel;1946-8-10
tt1417075;In Darkness;2011;Drama;Agnieszka;Holland;Polish;Poland;1976-11-13
tt2011971;Monsieur Lazhar;2011;Comedy;Philippe;Falardeau;French;Canada;1946-12-26
tt1615147;Margin Call;2011;Drama;J.C.;Chandor;English;USA;1969-7-23
tt1124035;The Ides of March;2011;Drama;George;Clooney;English;USA;1944-11-17
tt1965148;The Adventures of Tintin: The Secret of the Unicorn;2011;Adventure;Jacques;Exertier;French;France;1968-5-5
tt1204342;The Muppets;2011;Comedy;James;Bobin;English;USA;1962-1-18
tt1436562;Rio;2011;Animation;Carlos;Saldanha;English;USA;1960-9-23
tt1680310;Harry Potter and the Deathly Hallows: Part II;2011;Action;Matt;Birch;English;UK;1950-9-21
tt1521197;Anonymous;2011;Drama;Roland;Emmerich;English;UK;1942-11-17
tt1229822;Jane Eyre;2011;Drama;Cary;Fukunaga;English;UK;1949-4-29
tt1723121;We're the Millers;2013;Comedy;Rawson;Thurber;English;USA;1957-6-2
tt1399103;Transformers: Dark of the Moon;2011;Action;Michael;Bay;English;USA;1971-11-13
tt0780504;Drive;2011;Crime;Nicolas;Refn;English;USA;1979-12-12
tt0433035;Real Steel;2011;Action;Shawn;Levy;English;USA;1959-4-13
tt1318514;Rise of the Planet of the Apes;2011;Action;Rupert;Wyatt;English;USA;1980-1-31
tt1192628;Rango;2011;Animation;Gore;Verbinski;English;USA;1941-5-20
tt1673702;A Cat in Paris;2010;Animation;Jean-Loup;Felicioli;French;France;1962-1-14
tt1235830;Chico & Rita;2010;Animation;Tono;Errando;Spanish;Spain;1967-10-7
tt1302011;Kung Fu Panda 2;2011;Animation;Jennifer;Yuh;English;USA;1975-7-16
tt0448694;Puss in Boots;2011;Animation;Chris;Miller;English;USA;1970-3-26
tt1778342;The Fantastic Flying Books of Mr. Morris Lessmore;2011;Animation;William;Joyce;N/A;USA;1940-7-12
tt0146838;Any Given Sunday;1999;Drama;Oliver;Stone;English;USA;1961-9-4
tt1957945;La Luna;2011;Animation;Enrico;Casarosa;English;USA;1955-7-25
tt1964446;A Morning Stroll;2011;Animation;Grant;Orchard;English;UK;1967-12-9
tt0088402;The Wild Life;1984;Comedy;Art;Linson;English;USA;1973-4-5
tt1860355;Undefeated;2011;Documentary;Daniel;Lindsay;English;USA;1946-6-7
tt1748043;Hell and Back Again;2011;Documentary;Danfung;Dennis;English;USA;1954-12-3
tt1787725;If a Tree Falls: A Story of the Earth Liberation Front;2011;Documentary;Marshall;Curry;English;USA;1960-11-16
tt2028530;Paradise Lost 3: Purgatory;2011;Documentary;Joe;Berlinger;English;USA;1953-3-30
tt1440266;Pina;2011;Documentary;Wim;Wenders;German;Germany;1947-6-27
tt0384504;Saving Face;2004;Comedy;Alice;Wu;English;USA;1953-12-29
tt1844056;The Barber of Birmingham: Foot Soldier of the Civil Rights Movement;2011;Documentary;Gail;Dolgin;English;USA;1980-7-6
tt2210633;God Is the Bigger Elvis;2012;Documentary;Rebecca;Cammisa;English;USA;1962-4-8
tt1746180;Incident in New Baghdad;2011;Documentary;James;Spione;English;USA;1980-3-26
tt2028578;The Tsunami and the Cherry Blossom;2011;Documentary;Lucy;Walker;Japanese;N/A;1966-2-19
tt1155060;Down the Shore;2011;Drama;Harold;Guskin;English;USA;1974-8-8
tt1866218;Pentecost;2011;Short;Peter;McDonald;English;Ireland;1941-6-19
tt0173102;Raju Ban Gaya Gentleman;1992;Musical;Aziz;Mirza;Hindi;India;1944-6-22
tt1733689;Time Freak;2011;Short;Andrew;Bowler;English;USA;1973-6-9
tt2061843;Tuba Atlantic;2010;Short;Hallvar;Witzø;Norwegian;Norway;1958-9-29
tt1504320;The King's Speech;2010;Biography;Tom;Hooper;English;UK;1941-8-22
tt1285016;The Social Network;2010;Biography;David;Fincher;English;USA;1953-1-25
tt0964517;The Fighter;2010;Biography;David;Russell;English;USA;1949-7-23
tt0947798;Black Swan;2010;Drama;Darren;Aronofsky;English;USA;1970-5-4
tt1375666;Inception;2010;Action;Christopher;Nolan;English;USA;1965-12-14
tt0435761;Toy Story 3;2010;Animation;Lee;Unkrich;English;USA;1955-6-10
tt1542344;127 Hours;2010;Adventure;Danny;Boyle;English;USA;1956-10-16
tt1399683;Winter's Bone;2010;Drama;Debra;Granik;English;USA;1942-9-12
tt1403865;True Grit;2010;Adventure;Ethan;Coen;English;USA;1969-1-21
tt0842926;The Kids Are All Right;2010;Comedy;Lisa;Cholodenko;English;USA;1972-11-24
tt1164999;Biutiful;2010;Drama;Alejandro;Iñárritu;Spanish;Mexico;1949-9-27
tt0935075;Rabbit Hole;2010;Drama;John;Mitchell;English;USA;1963-9-17
tt1120985;Blue Valentine;2010;Drama;Derek;Cianfrance;English;USA;1953-5-13
tt0840361;The Town;2010;Crime;Ben;Affleck;English;USA;1943-12-27
tt1313092;Animal Kingdom;2010;Crime;David;Michôd;English;Australia;1964-10-22
tt1340107;In a Better World;2010;Drama;Susanne;Bier;Danish;Denmark;1946-8-27
tt1229381;Outside the Law;2010;Crime;Rachid;Bouchareb;French;France;1964-12-26
tt1255953;Incendies;2010;Drama;Denis;Villeneuve;French;Canada;1973-10-26
tt1379182;Dogtooth;2009;Drama;Yorgos;Lanthimos;Greek;Greece;1950-3-25
tt1431181;Another Year;2010;Comedy;Mike;Leigh;English;UK;1963-3-3
tt0892769;How to Train Your Dragon;2010;Animation;Dean;DeBlois;English;USA;1973-8-20
tt1555064;Country Strong;2010;Drama;Shana;Feste;English;USA;1950-1-28
tt0398286;Tangled;2010;Animation;Nathan;Greno;English;USA;1955-6-19
tt1014759;Alice in Wonderland;2010;Adventure;Tim;Burton;English;USA;1961-6-30
tt1571403;Harry Potter and the Deathly Hallows: Part I;2010;Fantasy;Matt;Birch;English;UK;1979-6-27
tt1226236;I Am Love;2009;Drama;Luca;Guadagnino;Italian;Italy;1969-1-2
tt1274300;The Tempest;2010;Comedy;Julie;Taymor;English;USA;1953-11-27
tt0034398;The Wolf Man;1941;Drama;George;Waggner;English;USA;1973-5-9
tt1423894;Barney's Version;2010;Comedy;Richard;Lewis;English;Canada;1974-5-31
tt1727388;The Way Way Back;2013;Comedy;Nat;Faxon;English;USA;1948-12-3
tt0944835;Salt;2010;Action;Phillip;Noyce;English;USA;1967-6-21
tt1104001;TRON: Legacy;2010;Action;Joseph;Kosinski;English;USA;1975-7-12
tt0477080;Unstoppable;2010;Action;Tony;Scott;English;USA;1944-4-22
tt1212419;Hereafter;2010;Drama;Clint;Eastwood;English;USA;1956-12-22
tt1228705;Iron Man 2;2010;Action;Jon;Favreau;English;USA;1969-7-20
tt0443543;The Illusionist;2006;Drama;Neil;Burger;English;USA;1969-6-23
tt1669698;The Lost Thing;2010;Animation;Andrew;Ruhemann;English;Australia;1970-11-7
tt0058182;A Hard Day's Night;1964;Comedy;Richard;Lester;English;UK;1960-11-30
tt1461418;The Gruffalo;2009;Animation;Max;Lang;English;UK;1940-2-22
tt1590114;Madagascar, a Journey Diary;2010;Animation;Bastien;Dubois;French;Fiji;1972-3-2
tt1470652;Let's Pollute;2009;Animation;Geefwee;Boedoe;English;USA;1950-12-13
tt1645089;Inside Job;2010;Documentary;Charles;Ferguson;English;USA;1950-3-14
tt1587707;Exit Through the Gift Shop;2010;Documentary;Banksy;@@@;English;UK;1949-10-1
tt1558250;GasLand;2010;Documentary;Josh;Fox;English;USA;1960-7-19
tt1559549;Restrepo;2010;Documentary;Tim;Hetherington;English;USA;1966-2-9
tt1268204;Waste Land;2010;Documentary;Lucy;Walker;English;Brazil;1962-5-15
tt1754549;Strangers No More;2010;Documentary;Karen;Goodman;English;USA;1954-7-30
tt1744887;The Warriors of Qiugang;2010;Documentary;Ruby;Yang;Chinese;USA;1968-1-10
tt1684897;Killing in the Name;2010;Documentary;Jed;Rothstein;English;USA;1978-2-10
tt1505384;Poster Girl;2010;Documentary;Sara;Nesson;English;USA;1953-4-15
tt1621975;Sun Come Up;2011;Documentary;Jennifer;Redfearn;English;USA;1976-9-7
tt1631323;God of Love;2010;Short;Luke;Matheny;N/A;USA;1973-8-12
tt1518330;Wish 143;2009;Short;Ian;Barnes;English;UK;1962-4-14
tt0182503;Turist Ömer Uzay Yolu'nda;1973;Comedy;Hulki;Saner;Turkish;Turkey;1962-4-9
tt2758880;Winter Sleep;2014;Drama;Nuri;Ceylan;Turkish;Turkey;1976-12-9
tt2692904;Locke;2013;Drama;Steven;Knight;English;UK;1948-6-24
tt2465578;Human Capital;2013;Drama;Paolo;Virzì;Italian;Italy;1965-10-22
tt2234003;Calvary;2014;Drama;John;McDonagh;English;Ireland;1975-7-28
tt0821638;Bury My Heart at Wounded Knee;2007;Drama;Yves;Simoneau;English;USA;1963-3-17
tt1181840;Jack and the Cuckoo-Clock Heart;2013;Animation;Stéphane;Berla;French;France;1942-7-22
tt2368672;Minuscule: Valley of the Lost Ants;2013;Animation;Hélène;Giraud;N/A;France;1947-5-21
tt3129484;Der Banker: Master of the Universe;2013;Documentary;Marc;Bauder;German;Austria;1949-6-22
tt2823088;Carmina y amén.;2014;Comedy;Paco;León;Spanish;Spain;1940-10-25
tt2392326;Le Week-End;2013;Comedy;Roger;Michell;English;UK;1974-10-8
tt1031254;Lost Boys: The Tribe;2008;Comedy;P.J.;Pesce;English;USA;1953-11-27
tt3114132;10.000 Km;2014;Comedy;Carlos;Marques-Marcet;Spanish;Spain;1957-1-19
tt0114095;Party Girl;1995;Comedy;Daisy;Mayer;English;USA;1963-7-20
tt0101414;Beauty and the Beast;1991;Animation;Gary;Trousdale;English;USA;1940-9-24
tt2113681;The 100-Year-Old Man Who Climbed Out the Window and Disappeared;2013;Adventure;Felix;Herngren;Swedish;Sweden;1942-4-22
tt2278871;Blue Is the Warmest Color;2013;Drama;Abdellatif;Kechiche;French;France;1940-8-30
tt0336055;Blancanieves y el príncipe azul;1945;Family;N/A;@@@;Spanish;Argentina;1962-7-16
tt0107730;Oh Boy!;1991;Comedy;Orlow;Seunke;Dutch;Netherlands;1964-11-16
tt1924396;The Best Offer;2013;Crime;Giuseppe;Tornatore;English;Italy;1951-7-31
tt1590013;The Last Sentence;2012;Biography;Jan;Troell;Swedish;Sweden;1967-7-8
tt1964624;In the House;2012;Comedy;François;Ozon;French;France;1956-12-2
tt1674773;Hannah Arendt;2012;Biography;Margarethe;Trotta;German;Germany;1950-2-12
tt2187115;Child's Pose;2013;Drama;Calin;Netzer;Romanian;Romania;1961-6-28
tt2219514;Fill the Void;2012;Drama;Rama;Burshtein;Hebrew;Israel;1944-2-1
tt0032910;Pinocchio;1940;Animation;Norman;Ferguson;English;USA;1967-2-4
tt0051011;Stopover Tokyo;1957;Drama;Richard;Breen;English;USA;1958-1-5
tt1854236;Love Is All You Need;2012;Comedy;Susanne;Bier;English;Denmark;1972-4-16
tt2243389;I'm So Excited!;2013;Comedy;Pedro;Almodóvar;Spanish;Spain;1966-6-13
tt2395421;The Priest's Children;2013;Comedy;Vinko;Bresan;Croatian;Croatia;1959-2-14
tt2767948;Benvenuto Presidente!;2013;Comedy;Riccardo;Milani;Italian;Italy;1965-11-11
tt1000734;Secret Diary of a Call Girl;2007–;Drama;N/A;@@@;English;UK;1961-1-12
tt2085002;Eat Sleep Die;2012;Drama;Gabriela;Pichler;Swedish;Sweden;1966-11-27
tt0090903;Il miele del diavolo;1986;Thriller;Lucio;Fulci;English;Spain;1961-12-31
tt0084509;The Plague Dogs;1982;Animation;Martin;Rosen;English;UK;1975-12-30
tt2178941;Barbara;2012;Drama;Christian;Petzold;German;Germany;1962-11-16
tt2177511;Caesar Must Die;2012;Drama;Paolo;Taviani;Italian;Italy;1980-8-11
tt1723811;Shame;2011;Drama;Steve;McQueen;English;UK;1964-10-3
tt1966594;Drew Peterson: Untouchable;2012;Drama;Mikael;Salomon;English;USA;1959-5-23
tt1827487;Once Upon a Time in Anatolia;2011;Crime;Nuri;Ceylan;Turkish;Turkey;1971-12-4
tt1660302;Our Children;2012;Drama;Joachim;Lafosse;French;Belgium;1957-6-24
tt1403214;Paradise: Love;2012;Drama;Ulrich;Seidl;German;Austria;1943-12-6
tt1692486;Carnage;2011;Comedy;Roman;Polanski;English;France;1941-6-20
tt2258281;Beyond the Hills;2012;Drama;Cristian;Mungiu;Romanian;Romania;1952-6-9
tt0016847;Faust;1926;Drama;F.W.;Murnau;German;Germany;1944-1-27
tt2036388;Shun Li and the Poet;2011;Drama;Andrea;Segre;Italian;Italy;1952-2-3
tt1924394;The Angels' Share;2012;Comedy;Ken;Loach;English;UK;1949-8-17
tt1374985;Alois Nebel;2011;Animation;Tomás;Lunák;Czech;Czech Republic;1979-5-21
tt1407052;Wrinkles;2011;Animation;Ignacio;Ferreras;Spanish;Spain;1941-2-27
tt1937419;London - The Modern Babylon;2012;Documentary;Julien;Temple;English;UK;1956-4-8
tt0344186;The Little Bird Boy;2002;Family;Rahbar;Ghanbari;Persian;Iran;1970-11-15
tt1729226;Teddy Bear;2012;Drama;Mads;Matthiesen;Danish;Denmark;1959-9-22
tt0412019;Broken Flowers;2005;Comedy;Jim;Jarmusch;English;USA;1946-8-13
tt2043962;Twilight Portrait;2011;Drama;Angelina;Nikonova;Russian;Russia;1966-4-4
tt0029476;Reported Missing;1937;Thriller;Milton;Carruth;English;USA;1964-8-4
tt1753887;Come as You Are;2011;Comedy;Geoffrey;Enthoven;Dutch;Belgium;1971-9-18
tt1412386;The Best Exotic Marigold Hotel;2011;Comedy;John;Madden;English;UK;1969-7-9
tt1614989;Headhunters;2011;Crime;Morten;Tyldum;Norwegian;Norway;1949-6-17
tt1441952;Salmon Fishing in the Yemen;2011;Comedy;Lasse;Hallström;English;UK;1974-6-16
tt1527186;Melancholia;2011;Drama;Lars;Trier;English;Denmark;1977-5-31
tt1827512;The Kid with a Bike;2011;Drama;Jean-Pierre;Dardenne;French;Belgium;1948-10-24
tt1508675;Le Havre;2011;Comedy;Aki;Kaurismäki;French;Finland;1950-5-9
tt1316540;The Turin Horse;2011;Drama;Béla;Tarr;Hungarian;Hungary;1949-8-18
tt1456472;We Have a Pope;2011;Comedy;Nanni;Moretti;Italian;Italy;1975-4-11
tt1242460;We Need to Talk About Kevin;2011;Drama;Lynne;Ramsay;English;UK;1955-10-31
tt1925421;Elena;2011;Drama;Andrey;Zvyagintsev;Russian;Russia;1967-4-30
tt1561768;Essential Killing;2010;Thriller;Jerzy;Skolimowski;English;Poland;1964-12-19
tt1189073;The Skin I Live In;2011;Thriller;Pedro;Almodóvar;Spanish;Spain;1958-6-2
tt1458175;The Next Three Days;2010;Crime;Paul;Haggis;English;USA;1964-9-30
tt1355638;The Rabbi's Cat;2011;Animation;Antoine;Delesvaux;French;France;1944-10-22
tt1742178;Position Among the Stars;2010;Documentary;Leonard;Helmrich;Indonesian;Netherlands;1954-8-28
tt0466650;Adem ile Havva;1986;Romance;Serif;Gören;Turkish;Turkey;1943-8-5
tt1220706;Breathing Room;2008;Horror;John;Suits;English;USA;1940-10-5
tt1503116;Nothing's All Bad;2010;Drama;Mikkel;Munch-Fals;Danish;Denmark;1980-3-18
tt0465538;Michael Clayton;2007;Crime;Tony;Gilroy;English;USA;1970-7-30
tt1620449;Animals United;2010;Animation;Reinhard;Klooss;German;Germany;1958-5-16
tt1440232;Little White Lies;2010;Comedy;Guillaume;Canet;French;France;1964-8-8
tt1422032;Even the Rain;2010;Drama;Icíar;Bollaín;Spanish;Spain;1944-12-21
tt1521848;Potiche;2010;Comedy;François;Ozon;French;France;1963-3-25
tt1529235;Welcome to the South;2010;Comedy;Luca;Miniero;Italian;Italy;1947-3-15
tt0414387;Pride & Prejudice;2005;Drama;Joe;Wright;English;France;1944-6-29
tt2170593;St. Vincent;2014;Comedy;Theodore;Melfi;English;USA;1962-1-10
tt0375912;Layer Cake;2004;Crime;Matthew;Vaughn;English;UK;1976-6-19
tt1126590;Big Eyes;2014;Biography;Tim;Burton;English;USA;1942-4-29
tt2980648;The Hundred-Foot Journey;2014;Comedy;Lasse;Hallström;English;India;1957-8-8
tt2172584;Maps to the Stars;2014;Drama;David;Cronenberg;English;Canada;1963-8-30
tt0075686;Annie Hall;1977;Comedy;Woody;Allen;English;USA;1980-7-27
tt2937898;A Most Violent Year;2014;Action;J.C.;Chandor;English;USA;1943-6-25
tt1959490;Noah;2014;Action;Darren;Aronofsky;English;USA;1958-9-14
tt3062880;Gett: The Trial of Viviane Amsalem;2014;Drama;Ronit;Elkabetz;Hebrew;Israel;1966-2-27
tt2262227;The Book of Life;2014;Animation;Jorge;Gutiérrez;English;USA;1962-12-11
tt0155267;The Thomas Crown Affair;1999;Crime;John;McTiernan;English;USA;1960-5-30
tt1606375;Downton Abbey;2010–;Drama;N/A;@@@;English;UK;1960-10-13
tt0944947;Game of Thrones;2011–;Adventure;N/A;@@@;English;USA;1973-10-8
tt1442462;The Good Wife;2009–;Crime;N/A;@@@;English;USA;1957-11-5
tt1856010;House of Cards;2013–;Drama;N/A;@@@;English;USA;1964-8-30
tt3502262;Transparent;2014–;Comedy;N/A;@@@;English;USA;1959-1-30
tt0377092;Mean Girls;2004;Comedy;Mark;Waters;English;USA;1954-7-11
tt3566726;Jane the Virgin;2014–;Comedy;N/A;@@@;English;USA;1972-7-6
tt2372162;Orange Is the New Black;2013–;Comedy;N/A;@@@;English;USA;1941-6-27
tt2575988;Silicon Valley;2014–;Comedy;N/A;@@@;English;USA;1955-4-2
tt2937900;The Knick;2014–;Drama;N/A;@@@;English;USA;1969-1-14
tt2249007;Ray Donovan;2013–;Crime;N/A;@@@;English;USA;1974-12-8
tt2741602;The Blacklist;2013–;Crime;N/A;@@@;English;USA;1957-3-17
tt1796960;Homeland;2011–;Drama;N/A;@@@;English;USA;1969-8-23
tt3205802;How to Get Away with Murder;2014–;Crime;N/A;@@@;English;USA;1958-8-18
tt1797404;House of Lies;2012–;Comedy;N/A;@@@;English;USA;1954-4-21
tt2616280;Derek;2012–;Comedy;N/A;@@@;English;UK;1964-6-9
tt1492966;Louie;2010–;Comedy;N/A;@@@;English;USA;1973-5-18
tt1586680;Shameless;2011–;Comedy;N/A;@@@;English;USA;1960-11-18
tt1190689;Nurse Jackie;2009–;Comedy;N/A;@@@;English;USA;1967-9-21
tt1759761;Veep;2012–;Comedy;N/A;@@@;English;USA;1955-6-9
tt0116282;Fargo;1996;Crime;Joel;Coen;English;USA;1965-10-3
tt0338188;The Missing;2003;Adventure;Ron;Howard;English;USA;1951-5-2
tt2356777;True Detective;2014–;Crime;N/A;@@@;English;USA;1970-9-13
tt1684226;The Normal Heart;2014;Drama;Ryan;Murphy;English;USA;1972-7-6
tt3012698;Olive Kitteridge;2014;Drama;N/A;@@@;English;USA;1973-4-23
tt3021686;The Honourable Woman;2014;Drama;N/A;@@@;English;UK;1980-5-18
tt0105477;Stop! Or My Mom Will Shoot;1992;Action;Roger;Spottiswoode;English;USA;1953-3-11
tt1979320;Rush;2013;Action;Ron;Howard;English;UK;1961-6-12
tt1967545;Labor Day;2013;Drama;Jason;Reitman;English;USA;1940-3-13
tt2390361;Enough Said;2013;Comedy;Nicole;Holofcener;English;USA;1966-3-4
tt2347569;Frances Ha;2012;Comedy;Noah;Baumbach;English;USA;1967-9-23
tt1951264;The Hunger Games: Catching Fire;2013;Adventure;Francis;Lawrence;English;USA;1952-8-9
tt1196956;One Chance;2013;Biography;David;Frankel;English;UK;1959-11-23
tt0124298;Blast from the Past;1999;Comedy;Hugh;Wilson;English;USA;1970-1-14
tt0903747;Breaking Bad;2008–2013;Crime;N/A;@@@;English;USA;1954-9-10
tt2137109;Masters of Sex;2013–;Drama;N/A;@@@;English;USA;1941-3-22
tt2467372;Brooklyn Nine-Nine;2013–;Comedy;N/A;@@@;English;USA;1950-4-23
tt0898266;The Big Bang Theory;2007–;Comedy;N/A;@@@;English;USA;1967-1-30
tt1442437;Modern Family;2009–;Comedy;N/A;@@@;English;USA;1962-5-2
tt1266020;Parks and Recreation;2009–2015;Comedy;N/A;@@@;English;USA;1948-8-15
tt2234222;Orphan Black;2013–;Action;N/A;@@@;English;Canada;1974-1-19
tt0465551;Notes on a Scandal;2006;Drama;Richard;Eyre;English;UK;1965-12-27
tt0367279;Arrested Development;2003–;Comedy;N/A;@@@;English;USA;1971-9-26
tt2338232;The Michael J. Fox Show;2013–;Comedy;N/A;@@@;English;USA;1960-5-22
tt1826940;New Girl;2011–;Comedy;N/A;@@@;English;USA;1958-1-8
tt1291580;Behind the Candelabra;2013;Biography;Steven;Soderbergh;English;USA;1943-7-31
tt2163315;Dancing on the Edge;2013;Drama;N/A;@@@;English;UK;1959-10-24
tt2103085;Top of the Lake;2013;Crime;N/A;@@@;English;Australia;1940-2-3
tt2372220;The White Queen;2013;Drama;N/A;@@@;English;UK;1953-6-15
tt1474684;Luther;2010–2015;Crime;N/A;@@@;English;UK;1973-8-9
tt1745862;Phil Spector;2013;Biography;David;Mamet;English;USA;1960-8-6
tt2709784;Burton and Taylor;2013;Biography;Richard;Laxton;English;UK;1973-3-30
tt0073440;Nashville;1975;Drama;Robert;Altman;English;USA;1967-10-4
tt0098067;Parenthood;1989;Comedy;Ron;Howard;English;USA;1962-8-8
tt1764183;Arbitrage;2012;Drama;Nicholas;Jarecki;English;USA;1978-1-31
tt2053425;Rust and Bone;2012;Drama;Jacques;Audiard;French;France;1940-9-19
tt1700844;The Deep Blue Sea;2011;Drama;Terence;Davies;English;USA;1948-10-1
tt1704573;Bernie;2011;Comedy;Richard;Linklater;English;USA;1947-3-21
tt1477855;Hyde Park on Hudson;2012;Biography;Roger;Michell;English;UK;1949-9-6
tt1441951;Quartet;2012;Comedy;Dustin;Hoffman;English;UK;1961-10-23
tt1535438;Hope Springs;2012;Comedy;David;Frankel;English;USA;1940-12-5
tt1496422;The Paperboy;2012;Drama;Lee;Daniels;English;USA;1941-9-26
tt1371111;Cloud Atlas;2012;Drama;Tom;Tykwer;English;Germany;1959-12-25
tt1591479;Act of Valor;2012;Action;Mike;McCoy;English;USA;1947-2-28
tt1389096;Stand Up Guys;2012;Comedy;Fisher;Stevens;English;USA;1980-6-26
tt1392170;The Hunger Games;2012;Adventure;Gary;Ross;English;USA;1961-1-3
tt0837562;Hotel Transylvania;2012;Animation;Genndy;Tartakovsky;English;USA;1967-6-29
tt1446192;Rise of the Guardians;2012;Animation;Peter;Ramsey;English;USA;1974-2-17
tt0979432;Boardwalk Empire;2010–2014;Crime;N/A;@@@;English;USA;1943-3-21
tt1870479;The Newsroom;2012–;Drama;N/A;@@@;English;USA;1961-3-10
tt1582350;Episodes;2011–;Comedy;N/A;@@@;English;USA;1965-11-4
tt1825133;Smash;2012–2013;Drama;N/A;@@@;English;USA;1973-7-3
tt0804503;Mad Men;2007–;Drama;N/A;@@@;English;USA;1960-11-2
tt0914387;Damages;2007–2012;Crime;N/A;@@@;English;USA;1955-7-17
tt0496424;30 Rock;2006–2013;Comedy;N/A;@@@;English;USA;1974-10-17
tt1848902;Game Change;2012;Biography;Jay;Roach;English;USA;1960-3-29
tt1985443;Hatfields & McCoys;2012;Drama;N/A;@@@;English;USA;1976-7-3
tt1093357;The Darkest Hour;2011;Action;Chris;Gorak;English;USA;1970-3-27
tt2239947;Political Animals;2012;Drama;N/A;@@@;English;USA;1960-8-30
tt0988045;Sherlock Holmes;2009;Action;Guy;Ritchie;English;USA;1951-11-1
tt0423455;Hemingway & Gellhorn;2012;Biography;Philip;Kaufman;English;USA;1948-7-25
tt4146008;American Horror Story: Asylum - Get Committed the Experience;2012;Short;Elizabeth;Knight;English;USA;1957-2-14
tt1832045;Magic City;2012–2013;Crime;N/A;@@@;English;USA;1965-4-6
tt1306980;50/50;2011;Comedy;Jonathan;Levine;English;USA;1974-1-20
tt1616195;J. Edgar;2011;Biography;Clint;Eastwood;English;USA;1973-9-7
tt1540133;The Guard;2011;Comedy;John;McDonagh;English;Ireland;1970-4-1
tt1625346;Young Adult;2011;Comedy;Jason;Reitman;English;USA;1948-12-18
tt1571222;A Dangerous Method;2011;Biography;David;Cronenberg;English;UK;1964-11-30
tt1586752;Machine Gun Preacher;2011;Action;Marc;Forster;English;USA;1946-1-15
tt1714209;In the Land of Blood and Honey;2011;Drama;Angelina;Jolie;Bosnian;USA;1971-9-28
tt1410063;The Flowers of War;2011;Drama;Yimou;Zhang;Mandarin;China;1940-8-28
tt1216475;Cars 2;2011;Animation;John;Lasseter;English;USA;1974-1-28
tt1430607;Arthur Christmas;2011;Animation;Sarah;Smith;English;UK;1949-9-5
tt0270980;My Boss's Daughter;2003;Comedy;David;Zucker;English;USA;1959-5-30
tt1509004;Enlightened;2011–2013;Comedy;N/A;@@@;English;USA;1977-11-9
tt1327801;Glee;2009–;Comedy;N/A;@@@;English;USA;1967-6-4
tt1582457;The Borgias;2011–2013;Crime;N/A;@@@;English;Hungary;1941-4-27
tt1637727;The Killing;2011–;Crime;N/A;@@@;English;USA;1975-1-28
tt0121766;Star Wars: Episode III - Revenge of the Sith;2005;Action;George;Lucas;English;USA;1943-8-27
tt0102517;Necessary Roughness;1991;Comedy;Stan;Dragoti;English;USA;1976-4-17
tt0904208;Californication;2007–2014;Comedy;N/A;@@@;English;USA;1948-11-12
tt1229413;Hung;2009–2011;Comedy;N/A;@@@;English;USA;1944-1-31
tt1515193;The Big C;2010–2013;Comedy;N/A;@@@;English;USA;1965-5-28
tt0037913;Mildred Pierce;1945;Crime;Michael;Curtiz;English;USA;1973-3-26
tt1742683;Too Big to Fail;2011;Drama;Curtis;Hanson;English;USA;1980-9-24
tt1623742;Cinema Verite;2011;Drama;Shari;Berman;English;USA;1955-10-27
tt1797469;Page Eight;2011;Drama;David;Hare;English;UK;1978-7-10
tt1831575;Appropriate Adult;2011;Drama;N/A;@@@;English;UK;1972-5-31
tt1245526;RED;2010;Action;Robert;Schwentke;English;USA;1974-10-10
tt1243957;The Tourist;2010;Action;Florian;Donnersmarck;English;USA;1973-7-9
tt1126591;Burlesque;2010;Drama;Steve;Antin;English;USA;1949-4-6
tt1194417;Casino Jack;2010;Biography;George;Hickenlooper;English;Canada;1972-6-26
tt1282140;Easy A;2010;Comedy;Will;Gluck;English;USA;1969-12-3
tt0980970;The Chronicles of Narnia: The Voyage of the Dawn Treader;2010;Adventure;Michael;Apted;English;USA;1963-1-8
tt0055862;Le concert de M. et Mme. Kabal;1962;Animation;Walerian;Borowczyk;N/A;France;1971-6-3
tt0317198;Bridget Jones: The Edge of Reason;2004;Comedy;Beeban;Kidron;English;UK;1975-4-24
tt1323594;Despicable Me;2010;Animation;Pierre;Coffin;English;USA;1973-5-15
tt0773262;Dexter;2006–2013;Crime;N/A;@@@;English;USA;1956-7-26
tt1520211;The Walking Dead;2010–;Drama;N/A;@@@;English;USA;1944-5-24
tt0412142;House M.D.;2004–2012;Drama;N/A;@@@;English;USA;1953-2-25
tt1124373;Sons of Anarchy;2008–;Crime;N/A;@@@;English;USA;1964-12-11
tt1495708;Covert Affairs;2010–;Action;N/A;@@@;English;USA;1969-3-15
tt0458253;The Closer;2005–2012;Crime;N/A;@@@;English;USA;1949-1-13
tt0386676;The Office;2005–2013;Comedy;N/A;@@@;English;USA;1976-11-13
tt1001482;United States of Tara;2009–2011;Comedy;N/A;@@@;English;USA;1950-7-29
tt1321865;Carlos;2010;Biography;N/A;@@@;English;France;1965-7-27
tt0374463;The Pacific;2010;Action;N/A;@@@;English;USA;1943-4-23
tt1453159;The Pillars of the Earth;2010;Drama;N/A;@@@;English;Germany;1974-12-15
tt1278469;Temple Grandin;2010;Biography;Mick;Jackson;English;USA;1940-1-31
tt1132623;You Don't Know Jack;2010;Biography;Barry;Levinson;English;USA;1979-8-23
tt1117646;The Special Relationship;2010;Drama;Richard;Loncraine;English;UK;1944-7-25
tt0974077;Cranford;2007–;Drama;N/A;@@@;English;UK;1948-12-23
tt0116191;Emma;1996;Comedy;Douglas;McGrath;English;UK;1970-1-17
tt2022170;The Client List;2012–;Drama;N/A;@@@;English;USA;1957-11-3
tt1600194;Hawaii Five-0;2010–;Action;N/A;@@@;English;USA;1980-10-8
