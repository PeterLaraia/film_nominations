Academy Awards;Best Picture;2015;tt2562232;Alejandro;Iñárritu;1959-2-11
Academy Awards;Best Director;2015;tt2562232;Alejandro;Iñárritu;1960-9-27
Academy Awards;Best Leading Actor;2015;tt2980516;James;Marsh;1958-3-1
Academy Awards;Best Leading Actress;2015;tt3316960;Richard;Glatzer;1950-12-14
Academy Awards;Best Supporting Actor;2015;tt2582802;Damien;Chazelle;1951-1-23
Academy Awards;Best Supporting Actress;2015;tt1065073;Richard;Linklater;1959-6-1
Academy Awards;Best Foreign Language Film;2015;tt2718492;Pawel;Pawlikowski;1970-3-23
Academy Awards;Best Original Screenplay;2015;tt2562232;Alejandro;Iñárritu;1950-1-15
Academy Awards;Best Adapted Screenplay;2015;tt2084970;Morten;Tyldum;1955-5-24
Academy Awards;Best Film Editing;2015;tt2582802;Damien;Chazelle;1956-1-5
Academy Awards;Best Cinematography;2015;tt2562232;Alejandro;Iñárritu;1947-10-19
Academy Awards;Best Music (Score);2015;tt2278388;Wes;Anderson;1947-7-19
Academy Awards;Best Song;2015;tt1020072;Ava;DuVernay;1956-9-18
Academy Awards;Best Art Direction;2015;tt2278388;Wes;Anderson;1941-1-21
Academy Awards;Best Costume Design;2015;tt2278388;Wes;Anderson;1970-12-2
Academy Awards;Best Makeup;2015;tt2278388;Wes;Anderson;1979-4-22
Academy Awards;Best Sound Mixing;2015;tt2582802;Damien;Chazelle;1951-3-26
Academy Awards;Best Sound Editing;2015;tt2179136;Clint;Eastwood;1978-8-12
Academy Awards;Best Visual Effects;2015;tt0816692;Christopher;Nolan;1977-3-27
Academy Awards;Best Animated Feature;2015;tt2245084;Don;Hall;1980-4-27
Academy Awards;Best Short Film - Animated;2015;tt0426459;John;Gulager;1980-5-12
Academy Awards;Best Documentary Feature;2015;tt4044364;Laura;Poitras;1943-7-22
Academy Awards;Best Documentary, Short Subjects;2015;tt3449252;Ellen;Kent;1973-4-7
Academy Awards;Best Short Film;2015;tt3071532;Mat;Kirkby;1955-10-1
Academy Awards;Best Picture;2014;tt2024544;Steve;McQueen;1955-10-23
Academy Awards;Best Director;2014;tt1454468;Alfonso;Cuarón;1971-3-29
Academy Awards;Best Leading Actor;2014;tt0790636;Jean-Marc;Vallée;1958-10-8
Academy Awards;Best Leading Actress;2014;tt2334873;Woody;Allen;1946-9-23
Academy Awards;Best Supporting Actor;2014;tt0790636;Jean-Marc;Vallée;1974-1-11
Academy Awards;Best Supporting Actress;2014;tt2024544;Steve;McQueen;1958-4-4
Academy Awards;Best Foreign Language Film;2014;tt2358891;Paolo;Sorrentino;1968-7-13
Academy Awards;Best Original Screenplay;2014;tt1798709;Spike;Jonze;1972-12-14
Academy Awards;Best Adapted Screenplay;2014;tt2024544;Steve;McQueen;1951-1-6
Academy Awards;Best Film Editing;2014;tt1454468;Alfonso;Cuarón;1980-2-7
Academy Awards;Best Cinematography;2014;tt1454468;Alfonso;Cuarón;1965-2-12
Academy Awards;Best Music (Score);2014;tt1454468;Alfonso;Cuarón;1958-2-10
Academy Awards;Best Song;2014;tt2294629;Chris;Buck;1962-12-1
Academy Awards;Best Art Direction;2014;tt1343092;Baz;Luhrmann;1977-3-7
Academy Awards;Best Costume Design;2014;tt1343092;Baz;Luhrmann;1974-2-6
Academy Awards;Best Makeup;2014;tt0790636;Jean-Marc;Vallée;1950-7-30
Academy Awards;Best Sound Mixing;2014;tt1454468;Alfonso;Cuarón;1961-4-15
Academy Awards;Best Sound Editing;2014;tt1454468;Alfonso;Cuarón;1965-10-14
Academy Awards;Best Visual Effects;2014;tt1454468;Alfonso;Cuarón;1960-10-28
Academy Awards;Best Animated Feature;2014;tt2294629;Chris;Buck;1958-9-26
Academy Awards;Best Short Film - Animated;2014;tt3043542;Alexandre;Espigares;1974-8-1
Academy Awards;Best Documentary Feature;2014;tt2396566;Morgan;Neville;1968-2-26
Academy Awards;Best Documentary, Short Subjects;2014;tt2924484;Malcolm;Clarke;1954-12-6
Academy Awards;Best Short Film;2014;tt3346410;Anders;Walter;1953-5-19
Academy Awards;Best Picture;2013;tt1024648;Ben;Affleck;1943-10-2
Academy Awards;Best Director;2013;tt0454876;Ang;Lee;1964-9-16
Academy Awards;Best Leading Actor;2013;tt0443272;Steven;Spielberg;1977-2-27
Academy Awards;Best Leading Actress;2013;tt1045658;David;Russell;1947-5-15
Academy Awards;Best Supporting Actor;2013;tt1853728;Quentin;Tarantino;1979-9-12
Academy Awards;Best Supporting Actress;2013;tt1707386;Tom;Hooper;1963-11-18
Academy Awards;Best Foreign Language Film;2013;tt1570728;Glenn;Ficarra;1960-1-3
Academy Awards;Best Original Screenplay;2013;tt1853728;Quentin;Tarantino;1966-10-15
Academy Awards;Best Adapted Screenplay;2013;tt1024648;Ben;Affleck;1973-5-20
Academy Awards;Best Film Editing;2013;tt1024648;Ben;Affleck;1958-7-29
Academy Awards;Best Cinematography;2013;tt0454876;Ang;Lee;1943-8-5
Academy Awards;Best Music (Score);2013;tt0454876;Ang;Lee;1962-9-10
Academy Awards;Best Song;2013;tt1074638;Sam;Mendes;1967-3-29
Academy Awards;Best Art Direction;2013;tt0443272;Steven;Spielberg;1954-11-28
Academy Awards;Best Costume Design;2013;tt1781769;Joe;Wright;1968-7-25
Academy Awards;Best Makeup;2013;tt1707386;Tom;Hooper;1971-1-18
Academy Awards;Best Sound Mixing;2013;tt1707386;Tom;Hooper;1971-1-8
Academy Awards;Best Sound Editing;2013;tt1074638;Sam;Mendes;1944-12-26
Academy Awards;Best Visual Effects;2013;tt0454876;Ang;Lee;1965-7-19
Academy Awards;Best Animated Feature;2013;tt1217209;Mark;Andrews;1966-2-11
Academy Awards;Best Short Film - Animated;2013;tt2388725;John;Kahrs;1977-10-31
Academy Awards;Best Documentary Feature;2013;tt2125608;Malik;Bendjelloul;1964-12-19
Academy Awards;Best Documentary, Short Subjects;2013;tt2123210;Sean;Fine;1977-11-26
Academy Awards;Best Short Film;2013;tt2088735;Shawn;Christensen;1979-11-11
Academy Awards;Best Picture;2012;tt1655442;Michel;Hazanavicius;1971-11-19
Academy Awards;Best Director;2012;tt1655442;Michel;Hazanavicius;1974-4-1
Academy Awards;Best Leading Actor;2012;tt1655442;Michel;Hazanavicius;1940-6-14
Academy Awards;Best Leading Actress;2012;tt1007029;Phyllida;Lloyd;1978-1-14
Academy Awards;Best Supporting Actor;2012;tt1532503;Mike;Mills;1957-7-20
Academy Awards;Best Supporting Actress;2012;tt1454029;Tate;Taylor;1961-1-14
Academy Awards;Best Original Screenplay;2012;tt1605783;Woody;Allen;1954-4-29
Academy Awards;Best Adapted Screenplay;2012;tt1033575;Alexander;Payne;1969-10-4
Academy Awards;Best Film Editing;2012;tt1568346;David;Fincher;1950-3-25
Academy Awards;Best Cinematography;2012;tt0970179;Martin;Scorsese;1945-2-27
Academy Awards;Best Music (Score);2012;tt1655442;Michel;Hazanavicius;1956-11-1
Academy Awards;Best Song;2012;tt1204342;James;Bobin;1962-1-18
Academy Awards;Best Art Direction;2012;tt0970179;Martin;Scorsese;1975-10-9
Academy Awards;Best Costume Design;2012;tt1655442;Michel;Hazanavicius;1941-6-30
Academy Awards;Best Makeup;2012;tt1007029;Phyllida;Lloyd;1966-10-16
Academy Awards;Best Sound Mixing;2012;tt0970179;Martin;Scorsese;1976-2-5
Academy Awards;Best Sound Editing;2012;tt0970179;Martin;Scorsese;1943-10-12
Academy Awards;Best Visual Effects;2012;tt0970179;Martin;Scorsese;1953-12-10
Academy Awards;Best Animated Feature;2012;tt1192628;Gore;Verbinski;1941-5-20
Academy Awards;Best Short Film - Animated;2012;tt1778342;William;Joyce;1940-7-12
Academy Awards;Best Documentary Feature;2012;tt1860355;Daniel;Lindsay;1946-6-7
Academy Awards;Best Documentary, Short Subjects;2012;tt0384504;Alice;Wu;1953-12-29
Academy Awards;Best Short Film;2012;tt1155060;Harold;Guskin;1974-8-8
Academy Awards;Best Picture;2011;tt1504320;Tom;Hooper;1941-8-22
Academy Awards;Best Director;2011;tt1504320;Tom;Hooper;1944-8-11
Academy Awards;Best Leading Actor;2011;tt1504320;Tom;Hooper;1974-7-2
Academy Awards;Best Leading Actress;2011;tt0947798;Darren;Aronofsky;1952-7-4
Academy Awards;Best Supporting Actor;2011;tt0964517;David;Russell;1940-12-9
Academy Awards;Best Supporting Actress;2011;tt0964517;David;Russell;1948-12-16
Academy Awards;Best Foreign Language Film;2011;tt1340107;Susanne;Bier;1946-8-27
Academy Awards;Best Original Screenplay;2011;tt1504320;Tom;Hooper;1967-8-29
Academy Awards;Best Adapted Screenplay;2011;tt1285016;David;Fincher;1947-12-5
Academy Awards;Best Film Editing;2011;tt1285016;David;Fincher;1954-4-20
Academy Awards;Best Cinematography;2011;tt1375666;Christopher;Nolan;1978-4-12
Academy Awards;Best Music (Score);2011;tt1285016;David;Fincher;1956-8-21
Academy Awards;Best Song;2011;tt0435761;Lee;Unkrich;1958-12-8
Academy Awards;Best Art Direction;2011;tt1014759;Tim;Burton;1961-6-30
Academy Awards;Best Costume Design;2011;tt1014759;Tim;Burton;1960-8-6
Academy Awards;Best Makeup;2011;tt0034398;George;Waggner;1973-5-9
Academy Awards;Best Sound Mixing;2011;tt1375666;Christopher;Nolan;1960-2-6
Academy Awards;Best Sound Editing;2011;tt1375666;Christopher;Nolan;1953-9-7
Academy Awards;Best Visual Effects;2011;tt1375666;Christopher;Nolan;1950-4-20
Academy Awards;Best Animated Feature;2011;tt0435761;Lee;Unkrich;1978-6-24
Academy Awards;Best Short Film - Animated;2011;tt1669698;Andrew;Ruhemann;1970-11-7
Academy Awards;Best Documentary Feature;2011;tt1645089;Charles;Ferguson;1950-3-14
Academy Awards;Best Documentary, Short Subjects;2011;tt1754549;Karen;Goodman;1954-7-30
Academy Awards;Best Short Film;2011;tt1631323;Luke;Matheny;1973-8-12
European Awards;Best European Film;2014;tt2718492;Pawel;Pawlikowski;1975-5-2
European Awards;Best European Director;2014;tt2718492;Pawel;Pawlikowski;1963-9-1
European Awards;Best European Actor;2014;tt2473794;Mike;Leigh;1957-5-2
European Awards;Best European Actress;2014;tt2737050;Jean-Pierre;Dardenne;1971-7-11
European Awards;Best European Screenplay;2014;tt2718492;Pawel;Pawlikowski;1951-5-6
European Awards;Best European Cinematographer;2014;tt2718492;Pawel;Pawlikowski;1953-11-11
European Awards;Best Documentary;2014;tt3129484;Marc;Bauder;1949-6-22
European Awards;Discovery Award;2014;tt1031254;P.J.;Pesce;1953-11-27
European Awards;People's Choice Award for Bes European Film;2014;tt2718492;Pawel;Pawlikowski;1972-8-1
European Awards;Best European Film;2013;tt2358891;Paolo;Sorrentino;1941-11-30
European Awards;Best European Director;2013;tt2358891;Paolo;Sorrentino;1973-10-11
European Awards;Best European Actor;2013;tt2358891;Paolo;Sorrentino;1960-10-31
European Awards;Best European Actress;2013;tt2024519;Felix;Groeningen;1975-6-23
European Awards;Best European Screenplay;2013;tt1964624;François;Ozon;1960-11-6
European Awards;Best European Cinematographer;2013;tt2219514;Rama;Burshtein;1944-2-1
European Awards;Best Documentary;2013;tt2375605;Joshua;Oppenheimer;1973-7-18
European Awards;Best European Comedy;2013;tt1854236;Susanne;Bier;1972-4-16
European Awards;Discovery Award;2013;tt0107730;Orlow;Seunke;1954-6-7
European Awards;Best European Film;2012;tt1570728;Glenn;Ficarra;1940-3-24
European Awards;Best European Director;2012;tt1570728;Glenn;Ficarra;1950-2-2
European Awards;Best European Actor;2012;tt1570728;Glenn;Ficarra;1970-2-26
European Awards;Best European Actress;2012;tt1570728;Glenn;Ficarra;1975-7-29
European Awards;Best European Screenplay;2012;tt0099810;John;McTiernan;1943-12-31
European Awards;Best European Cinematographer;2012;tt1723811;Steve;McQueen;1979-8-27
European Awards;Best European Composer;2012;tt1340800;Tomas;Alfredson;1955-9-24
European Awards;Best European Editor;2012;tt1723811;Steve;McQueen;1953-11-5
European Awards;Best European Production Designer;2012;tt1340800;Tomas;Alfredson;1962-11-19
European Awards;Best Animated European Film;2012;tt1374985;Tomás;Lunák;1979-5-21
European Awards;Discovery Award;2012;tt0344186;Rahbar;Ghanbari;1970-11-15
European Awards;People's Choice Award for Bes European Film;2012;tt1753887;Geoffrey;Enthoven;1971-9-18
European Awards;Best European Film;2011;tt1527186;Lars;Trier;1977-5-31
European Awards;Best European Director;2011;tt1340107;Susanne;Bier;1956-2-28
European Awards;Best European Actor;2011;tt1504320;Tom;Hooper;1948-1-27
European Awards;Best European Actress;2011;tt1242460;Lynne;Ramsay;1955-10-31
European Awards;Best European Screenplay;2011;tt1827512;Jean-Pierre;Dardenne;1979-6-13
European Awards;Best European Cinematographer;2011;tt1527186;Lars;Trier;1960-4-24
European Awards;Best European Composer;2011;tt1655442;Michel;Hazanavicius;1942-11-21
European Awards;Best European Editor;2011;tt1504320;Tom;Hooper;1950-5-7
European Awards;Best European Production Designer;2011;tt1527186;Lars;Trier;1945-4-18
European Awards;Best Animated European Film;2011;tt1235830;Tono;Errando;1978-11-2
European Awards;Best Documentary;2011;tt1440266;Wim;Wenders;1943-8-12
European Awards;Discovery Award;2011;tt0466650;Serif;Gören;1943-8-5
European Awards;People's Choice Award for Bes European Film;2011;tt1504320;Tom;Hooper;1975-2-2
Golden Globes;Best Picture - Drama;2015;tt1065073;Richard;Linklater;1973-12-30
Golden Globes;Best Picture - Comedy or Musical;2015;tt2278388;Wes;Anderson;1979-7-28
Golden Globes;Best Director;2015;tt1065073;Richard;Linklater;1946-6-29
Golden Globes;Best Leading Actor Drama;2015;tt2980516;James;Marsh;1942-7-2
Golden Globes;Best Leading Actress Drama;2015;tt3316960;Richard;Glatzer;1977-4-20
Golden Globes;Best Leading Actor - Comedy or Musical;2015;tt2562232;Alejandro;Iñárritu;1968-5-29
Golden Globes;Best Leading Actress - Comedy or Musical;2015;tt1126590;Tim;Burton;1951-8-5
Golden Globes;Best Supporting Actor in a Motion Picture;2015;tt2582802;Damien;Chazelle;1977-4-20
Golden Globes;Best Supporting Actress in a Motion Picture;2015;tt1065073;Richard;Linklater;1963-6-28
Golden Globes;Best Screenplay;2015;tt2562232;Alejandro;Iñárritu;1968-9-13
Golden Globes;Best Original Score;2015;tt2980516;James;Marsh;1952-7-12
Golden Globes;Best Original Song;2015;tt1020072;Ava;DuVernay;1951-2-10
Golden Globes;Best Foreign Language Film;2015;tt2802154;Andrey;Zvyagintsev;1943-4-7
Golden Globes;Best Animated Feature Film;2015;tt1646971;Dean;DeBlois;1960-11-5
Golden Globes;Best Television Series - Drama;2015;tt0155267;John;McTiernan;1960-5-30
Golden Globes;Best Television Series - Comedy or Musical;2015;tt3502262;N/A;@@@;1959-1-30
Golden Globes;Best Leading Actor in a TV Series - Drama;2015;tt1856010;N/A;@@@;1979-8-15
Golden Globes;Best Leading Actor in a TV Series - Drama;2015;tt0155267;John;McTiernan;1955-8-25
Golden Globes;Best Leading Actor in a TV Series - Musical Or Comedy;2015;tt3502262;N/A;@@@;1942-6-16
Golden Globes;Best Leading Actress in a TV Series - Comedy or Musical;2015;tt3566726;N/A;@@@;1949-6-11
Golden Globes;Best Mini-Series Or Motion Picture Made for TV;2015;tt0116282;Joel;Coen;1965-10-3
Golden Globes;Best Performance by an Actor in a Mini-series or Motion Picture Made for TV;2015;tt0116282;Joel;Coen;1945-8-21
Golden Globes;Best Performance by an Actress in a Mini-series or Motion Picture Made for TV;2015;tt3021686;N/A;@@@;1980-5-18
Golden Globes;Best Supporting Actor in a Series, Mini-Series or Motion Picture Made for TV;2015;tt1684226;Ryan;Murphy;1941-3-7
Golden Globes;Best Supporting Actress in a Supporting Role in a Series, Mini-Series or Motion Picture Made for TV;2015;tt1606375;N/A;@@@;1950-10-28
Golden Globes;Best Picture - Drama;2014;tt2024544;Steve;McQueen;1951-5-13
Golden Globes;Best Picture - Comedy or Musical;2014;tt1800241;David;Russell;1966-4-12
Golden Globes;Best Director;2014;tt1454468;Alfonso;Cuarón;1947-5-26
Golden Globes;Best Leading Actor Drama;2014;tt0790636;Jean-Marc;Vallée;1961-8-10
Golden Globes;Best Leading Actress Drama;2014;tt2334873;Woody;Allen;1969-2-3
Golden Globes;Best Leading Actor - Comedy or Musical;2014;tt0993846;Martin;Scorsese;1972-11-29
Golden Globes;Best Leading Actress - Comedy or Musical;2014;tt1800241;David;Russell;1945-5-13
Golden Globes;Best Supporting Actor in a Motion Picture;2014;tt0790636;Jean-Marc;Vallée;1950-7-23
Golden Globes;Best Supporting Actress in a Motion Picture;2014;tt1800241;David;Russell;1959-12-10
Golden Globes;Best Screenplay;2014;tt1798709;Spike;Jonze;1974-9-21
Golden Globes;Best Original Score;2014;tt2017038;J.C.;Chandor;1961-11-8
Golden Globes;Best Original Song;2014;tt2304771;Justin;Chadwick;1972-12-3
Golden Globes;Best Foreign Language Film;2014;tt2358891;Paolo;Sorrentino;1940-12-7
Golden Globes;Best Animated Feature Film;2014;tt2294629;Chris;Buck;1943-5-6
Golden Globes;Best Television Series - Drama;2014;tt0903747;N/A;@@@;1954-9-10
Golden Globes;Best Television Series - Comedy or Musical;2014;tt2467372;N/A;@@@;1950-4-23
Golden Globes;Best Leading Actor in a TV Series - Drama;2014;tt0903747;N/A;@@@;1977-1-17
Golden Globes;Best Leading Actor in a TV Series - Drama;2014;tt1856010;N/A;@@@;1967-11-13
Golden Globes;Best Leading Actor in a TV Series - Musical Or Comedy;2014;tt2467372;N/A;@@@;1959-4-11
Golden Globes;Best Leading Actress in a TV Series - Comedy or Musical;2014;tt1266020;N/A;@@@;1971-11-16
Golden Globes;Best Mini-Series Or Motion Picture Made for TV;2014;tt1291580;Steven;Soderbergh;1943-7-31
Golden Globes;Best Performance by an Actor in a Mini-series or Motion Picture Made for TV;2014;tt1291580;Steven;Soderbergh;1961-2-9
Golden Globes;Best Performance by an Actress in a Mini-series or Motion Picture Made for TV;2014;tt2103085;N/A;@@@;1956-5-1
Golden Globes;Best Supporting Actor in a Series, Mini-Series or Motion Picture Made for TV;2014;tt2249007;N/A;@@@;1949-1-3
Golden Globes;Best Supporting Actress in a Supporting Role in a Series, Mini-Series or Motion Picture Made for TV;2014;tt2163315;N/A;@@@;1951-2-20
Golden Globes;Best Picture - Drama;2013;tt1024648;Ben;Affleck;1954-1-8
Golden Globes;Best Picture - Comedy or Musical;2013;tt1707386;Tom;Hooper;1949-11-12
Golden Globes;Best Director;2013;tt1024648;Ben;Affleck;1970-5-31
Golden Globes;Best Leading Actor Drama;2013;tt0443272;Steven;Spielberg;1940-1-20
Golden Globes;Best Leading Actress Drama;2013;tt1790885;Kathryn;Bigelow;1956-6-29
Golden Globes;Best Leading Actor - Comedy or Musical;2013;tt1707386;Tom;Hooper;1965-10-15
Golden Globes;Best Leading Actress - Comedy or Musical;2013;tt1045658;David;Russell;1973-8-6
Golden Globes;Best Supporting Actor in a Motion Picture;2013;tt1853728;Quentin;Tarantino;1980-4-29
Golden Globes;Best Supporting Actress in a Motion Picture;2013;tt1707386;Tom;Hooper;1948-6-23
Golden Globes;Best Screenplay;2013;tt1853728;Quentin;Tarantino;1957-8-12
Golden Globes;Best Original Score;2013;tt0454876;Ang;Lee;1976-4-6
Golden Globes;Best Original Song;2013;tt1074638;Sam;Mendes;1966-6-29
Golden Globes;Best Foreign Language Film;2013;tt1570728;Glenn;Ficarra;1966-8-18
Golden Globes;Best Animated Feature Film;2013;tt1217209;Mark;Andrews;1978-6-8
Golden Globes;Best Television Series - Drama;2013;tt1796960;N/A;@@@;1946-10-23
Golden Globes;Best Television Series - Comedy or Musical;2013;tt0377092;Mark;Waters;1973-4-13
Golden Globes;Best Leading Actor in a TV Series - Drama;2013;tt1796960;N/A;@@@;1948-1-10
Golden Globes;Best Leading Actor in a TV Series - Drama;2013;tt1796960;N/A;@@@;1974-9-1
Golden Globes;Best Leading Actor in a TV Series - Musical Or Comedy;2013;tt1797404;N/A;@@@;1952-8-18
Golden Globes;Best Leading Actress in a TV Series - Comedy or Musical;2013;tt0377092;Mark;Waters;1948-5-3
Golden Globes;Best Mini-Series Or Motion Picture Made for TV;2013;tt1848902;Jay;Roach;1960-3-29
Golden Globes;Best Performance by an Actor in a Mini-series or Motion Picture Made for TV;2013;tt1985443;N/A;@@@;1944-9-16
Golden Globes;Best Performance by an Actress in a Mini-series or Motion Picture Made for TV;2013;tt1848902;Jay;Roach;1959-10-11
Golden Globes;Best Supporting Actor in a Series, Mini-Series or Motion Picture Made for TV;2013;tt1848902;Jay;Roach;1967-12-6
Golden Globes;Best Supporting Actress in a Supporting Role in a Series, Mini-Series or Motion Picture Made for TV;2013;tt1606375;N/A;@@@;1967-10-14
Golden Globes;Best Picture - Drama;2012;tt1033575;Alexander;Payne;1958-7-23
Golden Globes;Best Picture - Comedy or Musical;2012;tt1655442;Michel;Hazanavicius;1976-10-20
Golden Globes;Best Director;2012;tt0970179;Martin;Scorsese;1940-4-3
Golden Globes;Best Leading Actor Drama;2012;tt1033575;Alexander;Payne;1945-8-3
Golden Globes;Best Leading Actress Drama;2012;tt1007029;Phyllida;Lloyd;1948-7-8
Golden Globes;Best Leading Actor - Comedy or Musical;2012;tt1655442;Michel;Hazanavicius;1972-7-31
Golden Globes;Best Leading Actress - Comedy or Musical;2012;tt1655420;Simon;Curtis;1964-8-29
Golden Globes;Best Supporting Actor in a Motion Picture;2012;tt1532503;Mike;Mills;1980-6-17
Golden Globes;Best Supporting Actress in a Motion Picture;2012;tt1454029;Tate;Taylor;1953-4-9
Golden Globes;Best Screenplay;2012;tt1605783;Woody;Allen;1962-3-6
Golden Globes;Best Original Score;2012;tt1655442;Michel;Hazanavicius;1972-1-13
Golden Globes;Best Original Song;2012;tt1723121;Rawson;Thurber;1980-12-11
Golden Globes;Best Animated Feature Film;2012;tt1965148;Jacques;Exertier;1974-4-13
Golden Globes;Best Television Series - Drama;2012;tt1796960;N/A;@@@;1951-5-25
Golden Globes;Best Television Series - Comedy or Musical;2012;tt1442437;N/A;@@@;1942-2-12
Golden Globes;Best Leading Actor in a TV Series - Drama;2012;tt0270980;David;Zucker;1968-1-19
Golden Globes;Best Leading Actor in a TV Series - Drama;2012;tt1796960;N/A;@@@;1960-2-4
Golden Globes;Best Leading Actor in a TV Series - Musical Or Comedy;2012;tt1582350;N/A;@@@;1955-9-10
Golden Globes;Best Leading Actress in a TV Series - Comedy or Musical;2012;tt1509004;N/A;@@@;1957-2-13
Golden Globes;Best Mini-Series Or Motion Picture Made for TV;2012;tt1606375;N/A;@@@;1974-10-29
Golden Globes;Best Performance by an Actor in a Mini-series or Motion Picture Made for TV;2012;tt1474684;N/A;@@@;1953-11-8
Golden Globes;Best Performance by an Actress in a Mini-series or Motion Picture Made for TV;2012;tt0037913;Michael;Curtiz;1953-11-30
Golden Globes;Best Supporting Actor in a Series, Mini-Series or Motion Picture Made for TV;2012;tt0944947;N/A;@@@;1953-4-1
Golden Globes;Best Picture - Drama;2011;tt1285016;David;Fincher;1960-1-21
Golden Globes;Best Picture - Comedy or Musical;2011;tt0842926;Lisa;Cholodenko;1971-3-19
Golden Globes;Best Director;2011;tt1285016;David;Fincher;1972-4-25
Golden Globes;Best Leading Actor Drama;2011;tt1504320;Tom;Hooper;1941-4-25
Golden Globes;Best Leading Actress Drama;2011;tt0947798;Darren;Aronofsky;1945-3-29
Golden Globes;Best Leading Actor - Comedy or Musical;2011;tt1423894;Richard;Lewis;1945-4-17
Golden Globes;Best Leading Actress - Comedy or Musical;2011;tt0842926;Lisa;Cholodenko;1949-3-26
Golden Globes;Best Supporting Actor in a Motion Picture;2011;tt0964517;David;Russell;1976-3-7
Golden Globes;Best Supporting Actress in a Motion Picture;2011;tt0964517;David;Russell;1979-8-12
Golden Globes;Best Screenplay;2011;tt1285016;David;Fincher;1963-6-3
Golden Globes;Best Original Score;2011;tt1285016;David;Fincher;1961-11-17
Golden Globes;Best Original Song;2011;tt1126591;Steve;Antin;1946-10-26
Golden Globes;Best Foreign Language Film;2011;tt1340107;Susanne;Bier;1944-11-25
Golden Globes;Best Animated Feature Film;2011;tt0435761;Lee;Unkrich;1946-7-17
Golden Globes;Best Television Series - Drama;2011;tt0979432;N/A;@@@;1955-11-16
Golden Globes;Best Television Series - Comedy or Musical;2011;tt1327801;N/A;@@@;1970-5-19
Golden Globes;Best Leading Actor in a TV Series - Drama;2011;tt0979432;N/A;@@@;1954-4-14
Golden Globes;Best Leading Actor in a TV Series - Drama;2011;tt1124373;N/A;@@@;1964-12-11
Golden Globes;Best Leading Actor in a TV Series - Musical Or Comedy;2011;tt0898266;N/A;@@@;1977-6-29
Golden Globes;Best Leading Actress in a TV Series - Comedy or Musical;2011;tt1515193;N/A;@@@;1966-6-4
Golden Globes;Best Mini-Series Or Motion Picture Made for TV;2011;tt1321865;N/A;@@@;1965-7-27
Golden Globes;Best Performance by an Actor in a Mini-series or Motion Picture Made for TV;2011;tt1132623;Barry;Levinson;1976-3-24
Golden Globes;Best Performance by an Actress in a Mini-series or Motion Picture Made for TV;2011;tt1278469;Mick;Jackson;1942-3-5
Golden Globes;Best Supporting Actor in a Series, Mini-Series or Motion Picture Made for TV;2011;tt1327801;N/A;@@@;1940-7-7
Golden Globes;Best Supporting Actress in a Supporting Role in a Series, Mini-Series or Motion Picture Made for TV;2011;tt1327801;N/A;@@@;1960-12-11
Academy Awards;Best Foreign Language Film;2012;tt1832382;Asghar;Farhadi;1965-1-28
European Awards;Best Documentary;2012;tt2215113;Manuel;Stürler;1974-6-3
